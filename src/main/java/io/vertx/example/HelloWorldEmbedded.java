package io.vertx.example;

import io.vertx.core.Vertx;
import io.vertx.core.http.HttpServer;
import io.vertx.core.http.HttpServerResponse;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.web.Router;
import io.vertx.ext.web.handler.BodyHandler;


/**
 * @author <a href="http://tfox.org">Tim Fox</a>
 */
public class HelloWorldEmbedded {

    public static void main(String[] args) {

        SimpleLDAPAuthProvider provider = new SimpleLDAPAuthProvider(Vertx.vertx());
        HttpServer server = Vertx.vertx().createHttpServer();
        Router router = Router.router(Vertx.vertx());
        router.route().handler(BodyHandler.create());

        router.post("/ldap").handler(rc -> {
            JsonObject credentials = rc.getBodyAsJson();
            provider.authenticate(credentials, authFuture -> {
                if (authFuture.succeeded()) {
                    JsonObject jsonResponse = new JsonObject()
                            .put("status", authFuture.result());
                    rc.response().end(jsonResponse.toString());
                } else {
                    JsonObject jsonResponse = new JsonObject()
                            .put("status", false);
                    rc.response().end(jsonResponse.toString());
                }
            });
        });

        server.requestHandler(router::accept).listen(8080);
    }

}
